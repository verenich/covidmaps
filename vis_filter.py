import torch
from torchvision import models
from torchvision import transforms
import torchvision
import numpy as np
import mxutils
from mxutils import ImGrads
from torch import optim
import matplotlib.pyplot as plt
import sys
import cv2


# use torchsummary module to debug layers

FILTER = 250 # CHANGE AT WILL
ACT_WT = 0.5
UPSCALE = 30
FACTOR = 1.1
OPTIM_STEPS = 50
NEW_SIZE = 28
# normalize transform
normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
resize = transforms.Resize(NEW_SIZE)
# setup cuda device
device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
# create a schar filter (3x3) and an image gradient object
filter_ = mxutils.get_schar_filter_3()
grad_layer = ImGrads(filter_)


class Hook():
    def __init__(self, module, backward=False):
        if backward==False:
            self.hook = module.register_forward_hook(self.hook_fn)
        else:
            self.hook = module.register_backward_hook(self.hook_fn)
    def hook_fn(self, module, input, output):
        self.input = input
        self.output = output
    def close(self):
        self.hook.remove()


# define a function to compute gradient loss of an image
def gradient_loss(img, beta=1, device='cpu'):
    # move gradient layer to cuda
    grad_layer.to(device)
    # pass through grad layer and square
    grad_sq = grad_layer(img.unsqueeze(0))**2
    grad_loss = torch.pow(grad_sq.mean(), beta/2)
    return grad_loss

if __name__ == "__main__":
    # get a trained model
    #model = models.resnet152(pretrained=True)
    model = torch.load("models/xrayModel.pt")
    # freeze parameters
    for param in model.parameters():
        param.requires_grad_(False)

    model.eval()
    width = 28 
    height = 28
    updated_size = width * FACTOR
    # generate numpy random image
    img = np.single(np.random.uniform(0,1, (3, height, width)))
    # convert to tensor image (gradients True by default)
    
    img_tensor = normalize(torch.from_numpy(img)).requires_grad_(True)
    # move everyting to device
    model.to(device)
    img_tensor = img_tensor.to(device).detach().requires_grad_(True)
    

    print("IMAGE TENSOR IS LEAF: ", img_tensor.is_leaf)
   
    # set model to eval mode
    model.eval()
    # create hook for a resnet 152 model at last conv layer
    hook_output = Hook(model.layer3[2].relu)
    for upscale_epoch in range(UPSCALE+1):
        # create optimizer
        optimizer = optim.Adam([img_tensor], lr=0.2)

        for op_step in range(OPTIM_STEPS):
            optimizer.zero_grad()
            model(img_tensor.unsqueeze(0))
            # remove batch dim
            layer_out = hook_output.output
            rms = torch.pow((layer_out[0, FILTER]**2).mean(), 0.5)
            if torch.isnan(rms):
                print("NOT A NUMBER ERROR IN RMS")
                sys.exit()
            # pixel intensity
            pixel_intensity = torch.pow((img_tensor**2).mean(), 0.5)
            if torch.isnan(pixel_intensity):
                print("Pixel Intensity NaN Error")
                sys.exit()
            # image gradients
            image_gradients = gradient_loss(img_tensor, beta = 1, device=device)
            if torch.isnan(image_gradients):
                print("Image Gradients NaN Error")
                sys.exit()
            loss = -ACT_WT*rms + pixel_intensity + image_gradients
            # print activation at begining
            if upscale_epoch == 0:
                print('begin epoch {}, activation: {}'.format(upscale_epoch, rms))
            # calculate gradients
            loss.backward()
            # update
            optimizer.step()
        # display result
        image_result = mxutils.convert_image(img_tensor)
        # plt.imshow(image_result)
        # plt.title('End of upscale: {} '.format(upscale_epoch))
        # plt.show()
        
        # compute new size H = W = scaled_dim, default for transforms.resize is Image.BILINEAR
        print("upscale epoch ", upscale_epoch)
        if upscale_epoch == UPSCALE:
            # gray = mxutils.convert_to_grayscale(image_result)
            gray = cv2.cvtColor(image_result, cv2.COLOR_BGR2GRAY)
            # print("gray shape: ", gray.shape)
            plt.imshow(gray, cmap='gray')
            plt.title('End of upscale: {} '.format(upscale_epoch))
            plt.show()
        
        image_result = cv2.resize(image_result, dsize=(0,0), fx=FACTOR, fy=FACTOR).transpose(2,0,1)

        # convert to PIL image, remove 0 dim
        # pil = transforms.ToPILImage()(img_tensor.cpu().squeeze(0))
        # pil = resize(pil)
        # convert back to tensor
        #image_result = mxutils.convert_image(transforms.ToTensor)
        # image_result = np.array(pil)
        # normalize larger image and convert to tensor
        img_tensor = normalize(torch.from_numpy(image_result)).requires_grad_(True)
        # move to device
        img_tensor = img_tensor.to(device).detach().requires_grad_(True)
            
    
