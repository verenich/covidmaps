# Scaled Directed Divergence Artifact Repository

This is an artifact repository for our work on the Scaled Directed Divergence kernel, a method to extract relevant activated regions of a given class in class overlapped environments.

## Reproducibility
1. Clone the repository localy with Pytorch installed.
2. If you would like to experiment with saliency maps provided by <code>flashtorch</code> use <code>pip install flashtorch</code>, this is strictly for experimentation and is not required for SDD (our method).
3. To run experiments on <code>car</code> images, use <code>saliency.py</code> file.
4. To run experiments on <code>X-ray</code> images, use <code>xray_sdd.py</code>
5. To visualize individual filters (what patterns they learn), use <code>vis_filter.py</code>, see below for example filter image.

## NOTE: if you get an error on normalization, see below regarding the flashtorch library, just comment out the code below from both files

```python
backdrop.visualize(img_tensor_, target_class, guided=True, use_gpu=True)
    plt.show()
```

## Pulmonary Radiology Domain (COVID-19 and Viral Pneumonia)
<details>
  <summary markdown="span">CAMs generated from a X-ray trained CNN, SDD extracts target features</summary>
  First 3 images show Class Activation Maps generated from a CNN trained to classify COVID-19, NORMAL, and Viral Pneumonia classes. We are interested in extracting COVID-19 related features (i.e. our TARGET class).  Scaled Directed Divergence is used to extract these features in the 4th image.
</details>

![](output/COVID_1.png)
![](output/COVID_2.png)

<details>
  <summary markdown="span">In this case Viral Pneumonia is the TARGET class</summary>
  Here we extract Pneumonia relevant activations from a test image that was classifed as Pneumonia
</details>

![](output/PNEUM_1.png)


## Natural Imagery Domain using a pre-trained model on ImageNet (ResNet152)
ImageNet classes by their <b>index number</b> for experimentation:

- 479 -> 'car wheel' 
- 751 -> 'racer, race car, racing car'
- 817 -> 'sports car, sport car'

First three images in each row show Class Activation Maps for their respective classes. It can be seen that they are very similar with significant overlap between these classes. Our goal is to extract activations relevant to our target class 479 (car wheel), which is shown in the 4th image of each row.  Please not that the CNN model has not been trained or fine-tuned by us, and is the standard pre-trained model available in Pytorch.  This specific model is ResNet152 and can be loaded directly from Pytorch.
<details>
  <summary markdown="span">Extract CAR WHEEL from CAR (2 car classes in ImageNet)</summary>
  Our goal is to extract features of our target class 'car wheel' from similar overlapping classes, which are 751 (race car) and 817 (sport car).
</details>

![](output/Figure_1.png)
![](output/Figure_2.png)
![](output/Figure_3.png)


## Sample of the saliency maps method (not our method) picking up spurious features when looking for a car wheel
Here a ROAD SIGN is picked up, as it highly correlates with cars and car wheel class.  This can be replicated in our code, or can be disabled by commenting out this code:
```python
backdrop.visualize(img_tensor, target_class, guided=True, use_gpu=True)
    plt.show()
```

![](output/Figure_S.png)


## NOTE: if saliency maps visualization provided by the flashtorch library is required, see below


In our experiments, saliency maps using the flashtorch library were performed. It can be found here:
https://github.com/MisaOgura/flashtorch

Staring with Pytorch 1.7, the library will report an error when running a denormalization operation. This can be fixed with the following change to the following __init__.py file after you install flashtorch using pip:

1. go to <code>flash/torch</code> directory of your flashtorch install
2. open the <code>__init__.py</code> file using a text editor
3. find the following code starting around line 119:
```python
denormalized = tensor.clone()
for channel, mean, std in zip(denormalized[0], means, stds):
    channel.mul_(std).add_(mean)

return denormalized
```

4. replace with this code:
```python
denormalized = tensor.clone()
with torch.no_grad():
    for channel, mean, std in zip(denormalized[0], means, stds):
        channel.mul_(std).add_(mean)

return denormalized.cpu()
```
## Network filter visualization can be done using the vis_filter.py file.

Change the filter number as such:
```python
FILTER = 250 # CHANGE AT WILL
```

Example pattern that this filter learns (upsampled to larger size)
![](output/FILER250.png)
