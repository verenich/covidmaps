import torch
import os
from torchvision import transforms
import torchvision
from PIL import Image
import io
import matplotlib.pyplot as plt
import numpy as np
import scipy.ndimage as nd

from flashtorch.utils import apply_transforms, load_image
from flashtorch.saliency import Backprop

from flashtorch.activmax import GradientAscent


device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
IMAGE_NET_MEAN = [0.485, 0.456, 0.406]
IMAGE_NET_STD = [0.229, 0.224, 0.225]

class Hook():
    def __init__(self, module, backward=False):
        if backward==False:
            self.hook = module.register_forward_hook(self.hook_fn)
        else:
            self.hook = module.register_backward_hook(self.hook_fn)
    def hook_fn(self, module, input, output):
        self.input = input
        self.output = output
    def close(self):
        self.hook.remove()
# tranform image bytes to pytorch tensor format
# Utility method to transform loaded image bytes to transformed input
def transform_image(image_bytes):
  mt = transforms.Compose([
    transforms.Resize(224),
    transforms.CenterCrop(224),
    transforms.ToTensor(),
    transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
  ])
  image = Image.open(io.BytesIO(image_bytes))
  # some samples may have more or less than 3 channels, convert to 3
  if len(image.getbands()) != 3:
      image = image.convert('RGB')
  tinput = mt(image)
  return tinput.unsqueeze(0)



def get_class_cam(model, outputs,activations, class_index):
    # remove batch and transpose to 7,7,2048
    out_features = activations.output.squeeze(0)
    out_features = np.transpose(out_features.cpu().detach(), (1,2,0))
    fw = model.fc.weight[class_index,:]
    cam = np.dot(out_features.detach(), fw.detach().cpu())
    return cam

def show_sub_plots(original, activations, pclasses):
    n = len(activations)
    f, axarr = plt.subplots(1,(n+1))
    inp = torch.squeeze(original, 0)
    inp = inp.numpy().transpose((1, 2, 0))
    mean = np.array(IMAGE_NET_MEAN)
    std = np.array(IMAGE_NET_STD)
    inp = std * inp + mean
    inp = np.clip(inp, 0, 1)
    i = 0
    while i < n:
        # turn axis off
        axarr[i].axis('off')
        axarr[i].imshow(inp, cmap='jet', alpha=1)
        axarr[i].set_title(pclasses[i])
        i += 1
    # add the original without cams
    axarr[i].axis('off')
    axarr[i].imshow(inp, cmap='jet', alpha=1)
    axarr[i].set_title('ORIGINAL')
    # scale and collect activation maps to size of original image
    #scaled = []
    col = 0
    for cam in activations:
        zcam = nd.zoom(cam, zoom=(32,32), order=1)
        #scaled.append(zcam)
        axarr[col].imshow(zcam, cmap='seismic', alpha=0.4)
        col += 1
    # pause to let render
    plt.pause(0.001)

def show_sub_plots_op(original, activations, pclasses):
    n = len(activations)
    f, axarr = plt.subplots(1,n)
    inp = torch.squeeze(original, 0)
    inp = inp.numpy().transpose((1, 2, 0))
    mean = np.array(IMAGE_NET_MEAN)
    std = np.array(IMAGE_NET_STD)
    inp = std * inp + mean
    inp = np.clip(inp, 0, 1)
    i = 0
    while i < n:
        # turn axis off
        axarr[i].axis('off')
        axarr[i].imshow(inp, cmap='jet', alpha=1)
        axarr[i].set_title(pclasses[i])
        i += 1
    # add the original without cams
    # axarr[i].axis('off')
    # axarr[i].imshow(inp, cmap='jet', alpha=1)
    # axarr[i].set_title('Input')
    # scale and collect activation maps to size of original image
    #scaled = []
    col = 0
    for cam in activations:
        zcam = nd.zoom(cam, zoom=(32,32), order=1)
        #scaled.append(zcam)
        axarr[col].imshow(zcam, cmap='seismic', alpha=0.4)
        col += 1
    # pause to let render
    plt.pause(0.001)



if __name__ == '__main__':

   
    xray_classes = ["COVID-19", "NORMAL", "VPNEUMONIA"]
    # model one
    COVID_MODEL = 'models/covidSGD.pt'
    #COVID_MODEL = 'models/xrayModel.pt'
    # COVID_MODEL = 'models/resnetCovidNet.pt'
    model = torch.load(COVID_MODEL)
    #model = torchvision.models.resnet152(pretrained=True)
    #model.eval()
    #model.to(device)

    backdrop = Backprop(model)
    
    

    # ITERATE THROUGH TEST FILES
    SOURCE_DIR = 'data/covid/test/COVID-19'
    FILES = os.listdir(SOURCE_DIR)

    target_class = 2

    #image = load_image("data/covid/test/COVID-19/COVID-19 (61).png")
    # image = load_image("data/covid/test/COVID-19/COVID-19(180).png")
    image = load_image("data/covid/test/VPNEUMONIA/Viral Pneumonia (119).png")
    # car6 for diff in saliency and CAMs
    #image = load_image("data/car6.jpeg")
        # show image
        # plt.imshow(image)
        # plt.show()

    img_tensor = apply_transforms(image)
    #img_tensor = transform_image(image)
    #img_tensor = img_tensor.to(device).detach().requires_grad_(True)
    grads = backdrop.calculate_gradients(img_tensor,target_class=target_class,take_max=True, guided=True, use_gpu=True)

    print('GRADS: \n', grads.shape)

    img_tensor_ = img_tensor.clone().cuda()

    backdrop.visualize(img_tensor_, target_class, guided=True, use_gpu=True)
    plt.show()

    hook = Hook(model.layer4[2].relu)
    output = model(img_tensor.cuda())

    cams = []
    pclasses = []
    # car wheel = 479
    # car mirror = 475
    cam1 = get_class_cam(model, output, hook, class_index=0)
    cams.append(cam1)
    pclasses.append('COVID')
    cam2 = get_class_cam(model, output, hook, class_index=1)
    cams.append(cam2)
    pclasses.append('NORM')
    cam3 = get_class_cam(model, output, hook, class_index=2)
    cams.append(cam3)
    pclasses.append('PNEUM')

    # subtract covid - prenum
    cam1_norm = cam1 / cam1.max()
    cam2_norm = cam2 / cam2.max()
    cam3_norm = cam3 / cam3.max()
    # diff = cam1_norm - cam3_norm
    # diff = diff - cam2_norm
    # try non-normalized activations because it's the same model
    print("CAM 1: \n", cam1)
    print('CAM 2: \n', cam2)
    print('CAM 3: \n', cam3)
    scale = 3 # number of divergent classes
    cam3 = cam3 * scale
    diff = cam3 - cam1
    diff = diff - cam2
    diff = np.exp(0.2 * diff)
    cams.append(diff)

    pclasses.append('PNEUM K')

    original = img_tensor.detach().cpu()
    show_sub_plots_op(original, cams, pclasses)
    plt.show()



        

            