# Copyright <2020> Edward Verenich <verenie@clarkson.edu>
# MIT license <https://opensource.org/licenses/MIT>
# A convolutional layer to penalize sharp changes in image values
# by using Scharr or Sobel filters
from __future__ import absolute_import
import torch.nn as nn
import torch
import numpy as np
from torchvision import transforms

# normalize and denormolize using ImageNet mean and standard deviation
normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
denormalize = transforms.Normalize(mean=[-0.485/0.229, -0.456/0.224, -0.406/0.225], std = [1/0.229, 1/0.224, 1/0.225])

class ImGrads(nn.Module):
    def __init__(self, weight):
        super().__init__()
        k_h, k_w = weight.shape[1:]
        # H and W of kernels should be odd, i.e. 3x3, 5x5
        padding_x = int((k_h - 1) / 2)
        padding_y = int((k_w - 1) / 2)
        # conv layer with 3 in-channels (RGB) and 6 out-channels (2 out (x,y grads) for each in-channel)
        self.conv = nn.Conv2d(3, 6, (k_h, k_w), bias=False, padding=(padding_x, padding_y))
        # init conv layer weights
        # derivatives for channel 1
        w_1x = np.array([weight[0], np.zeros((k_h, k_w)), np.zeros((k_h, k_w))])
        w_1y = np.array([weight[1], np.zeros((k_h, k_w)), np.zeros((k_h, k_w))])
        # derivatives for channel 2
        w_2x = np.array([np.zeros((k_h, k_w)), weight[0], np.zeros((k_h, k_w))])
        w_2y = np.array([np.zeros((k_h, k_w)), weight[1], np.zeros((k_h, k_w))])
        # derivatives for channel 3
        w_3x = np.array([np.zeros((k_h, k_w)), np.zeros((k_h, k_w)), weight[0]])
        w_3y = np.array([np.zeros((k_h, k_w)), np.zeros((k_h, k_w)), weight[1]])
        # create a final weight tensor
        weight_final = torch.from_numpy(np.array([w_1x, w_1y, w_2x, w_2y, w_3x, w_3y])).type(torch.FloatTensor)
        # make sure dimensions are correct
        if self.conv.weight.shape == weight_final.shape:
            self.conv.weight = nn.Parameter(weight_final)
            self.conv.weight.requires_grad_(False)
        else:
            print("Error: weights provided have wrong dimenstions, need shape: \n", self.conv.weight.shape )

    def forward(self, x):
        return self.conv(x)

# get a 3x3 Schar filter
def get_schar_filter_3():
    filter_x = np.array([[-3, 0, 3], [-10, 0, 10], [-3, 0, 3]])
    # transpose for y filter
    filter_y = filter_x.T
    return np.array([filter_x, filter_y])

# get a 3x3 Sobel filter
def get_sobel_filter_3():
    filter_x = np.array([[1, 0, -1], [2, 0, -2], [1, 0, -1]])
    # transpose for y filter
    filter_y = filter_x.T
    return np.array([filter_x, filter_y])

# returns a torch tensor with gradients enabled
def normalize_image(img, gradients=True):
    return normalize(torch.from_numpy(img)).requires_grad_(True)

# returns a numpay array for the image, transposed (1, 2, 0) if True    
def denormalize_image(img_tensor, transpose=False):
    img_cpu = img_tensor.cpu()
    img_cpu = denormalize(img_cpu.clone().detach()).numpy()
    if transpose:
        img_cpu = img_cpu.transpose(1,2,0)

    return img_cpu

# convert input tensor for matplotlib
def convert_image(img_tensor, transpose=True):
    np_image = denormalize_image(img_tensor, transpose)
    # clip negative values
    np_image = np_image.clip(0, 1)
    return np_image

# convert image array to grayscale
def convert_to_grayscale(img_arr):
    gray_image = np.sum(np.abs(img_arr), axis=0)
    img_max = np.percentile(gray_image, 99)
    img_min = np.min(gray_image)
    gray_image = (np.clip((gray_image - img_min)/ (img_max - img_min), 0, 1))
    gray_image = np.expand_dims(gray_image, axis=0)
    return gray_image

if __name__ == "__main__":
    # Scharr Filters
    # from mxutils import ImGrads
    filter_x = np.array([[-3, 0, 3], 
                     [-10, 0, 10],
                     [-3, 0, 3]])
    filter_y = filter_x.T
    grad_filters = np.array([filter_x, filter_y])

    grad_layer = ImGrads(grad_filters)
    print(grad_layer)

